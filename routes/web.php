<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CategoryGroupController;
use App\Http\Controllers\Admin\OfferController;
use App\Http\Controllers\Admin\PointsTransactionController;
use App\Http\Controllers\Admin\ProfilesController;
use App\Http\Controllers\Admin\SubscriptionController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Auth\ProfileAndUserController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function (){
    Route::group(['prefix' => 'auth', 'as' => 'auth.'], function (){
        Route::view('/profile/create', 'auth.profiles.create')->name('profile.create');
        Route::post('/profile', [ProfileAndUserController::class, 'store'])->name('profile.store');

        Route::group(['middleware' => 'HasProfile'], function (){
            Route::view('/profile/edit', 'auth.editProfile')->name('profile.edit');
            Route::put('/profile/{profile}/update', [ProfileAndUserController::class, 'updateProfile'])->name('profile.updateProfile');
            Route::put('/login/{user}/update', [ProfileAndUserController::class, 'updateLogin'])->name('profile.updateLogin');
            Route::put('/password/{user}/update', [ProfileAndUserController::class, 'updatePassword'])->name('profile.updatePassword');
        });
    });

    Route::group(['middleware' => ['role:Admin'], 'prefix' => 'admin', 'as' => 'admin.'], function(){
        Route::post('/user/{user}/changeRole', [UsersController::class, 'changeRole'])->name('users.changeRole');
        Route::post('/user/{user}/changeStatus', [UsersController::class, 'changeStatus'])->name('users.changeStatus');
        Route::resource('/users', UsersController::class, ['except' => ['create', 'store']]);
        Route::resource('/profiles', ProfilesController::class, ['only' => ['store', 'update', 'create']]);
        Route::resource('/category_groups', CategoryGroupController::class, ['except' => ['create']]);
        Route::post('/categories/{category_group}/', [CategoryController::class, 'store'])->name('categories.store');
        Route::resource('/categories', CategoryController::class, ['except' => ['index', 'create', 'show', 'store']]);
        Route::delete('/offers/delete/{media}', [OfferController::class, 'deleteMedia'])->name('offers.deleteMedia');
        Route::resource('/offers', OfferController::class, ['except' => ['create', 'store']]);
        Route::resource('/pointsTransactions', PointsTransactionController::class, ['except' => ['create', 'store']]);
        Route::get('/subscriptions', [SubscriptionController::class, 'index'])->name('subscriptions.index');
   });
    Route::post('/getsubscription/{subscription}', [SubscriptionController::class, 'getSubscription'])->name('getsubscription');
    Route::post('/pointstransfer/{user}', [PointsTransactionController::class, 'transfer'])->name('points.transfer');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



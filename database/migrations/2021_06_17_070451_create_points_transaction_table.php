<?php

use App\Models\PointsTransaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointsTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points_transaction', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->enum('transaction_type', PointsTransaction::$pointsTransactionTypes);
            $table->string('transfer_to')->nullable();
            $table->string('transfer_from')->nullable();
            $table->timestamp('completed_at')->nullable();
            $table->string('description')->nullable();
            $table->foreignId('offer_id')->nullable()->constrained('offers');
            $table->integer('amount');
            $table->boolean('subscription')->default(0)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points_transaction');
    }
}

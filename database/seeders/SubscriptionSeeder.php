<?php

namespace Database\Seeders;

use App\Models\Subscription;
use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subscription::create([
           'name' => 'Basic',
           'price' => 1000,
           'points' => 30,
        ]);
        Subscription::create([
            'name' => 'Pro',
            'price' => 5000,
            'points' => 50,
        ]);
    }
}

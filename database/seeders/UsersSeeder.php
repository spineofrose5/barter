<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::insert([
            'name' => 'Admin',
            'email' => 'admin@barter.test',
            'password' => bcrypt('password')
        ]);

        User::factory()->count(10)->create();

        User::find('1')->setStatus('Active');

        User::find('1')->assignRole(Role::findById('1'));
        User::find('2')->assignRole(Role::findById('2'));

        $users = User::where('id', '>', '2')->get();

        foreach ($users as $user) {
            $user->assignRole(Role::findById('3'));
        }
    }
}

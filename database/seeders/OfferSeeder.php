<?php

namespace Database\Seeders;

use App\Models\Offer;
use Illuminate\Database\Seeder;

class OfferSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Offer::factory()->count(200)->create();
        $offer = Offer::where('id', 1)->first();
        $offer->addMediaFromUrl('https://source.unsplash.com/random/200x200')->toMediaCollection('offer_images');
        $offer->addMediaFromUrl('https://source.unsplash.com/random/200x200')->toMediaCollection('offer_images');
        $offer->addMediaFromUrl('https://source.unsplash.com/random/200x200')->toMediaCollection('offer_images');
    }
}

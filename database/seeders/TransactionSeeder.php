<?php

namespace Database\Seeders;

use App\Models\PointsTransaction;
use App\Models\User;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PointsTransaction::create([
            'user_id' => '1',
            'transaction_type' => 'Incoming',
            'transfer_to' => null,
            'transfer_from' => null,
            'completed_at' => now(),
            'description' => 'Points from subscription',
            'offer_id' => null,
            'amount' => 50,
            'subscription' => 1
        ]);
        PointsTransaction::find('1')->setStatus('Success');

        PointsTransaction::create([
            'user_id' => '1',
            'transaction_type' => 'Outgoing',
            'transfer_to' => User::find(2)->name,
            'transfer_from' => null,
            'completed_at' => now(),
            'description' => 'Payment for the service',
            'offer_id' => 2,
            'amount' => 20,
        ]);
        PointsTransaction::find('2')->setStatus('Success');

        PointsTransaction::create([
            'user_id' => '2',
            'transaction_type' => 'Incoming',
            'transfer_to' => null,
            'transfer_from' => User::find(1)->name,
            'completed_at' => now(),
            'description' => 'Payment for the service',
            'offer_id' => 2,
            'amount' => 20,
        ]);
        PointsTransaction::find('3')->setStatus('Success');

        PointsTransaction::create([
            'user_id' => '3',
            'transaction_type' => 'Outgoing',
            'transfer_to' => User::find(2)->name,
            'transfer_from' => null,
            'completed_at' => null,
            'description' => 'Payment for the service - pending for approval',
            'offer_id' => 2,
            'amount' => 20,
        ]);
        PointsTransaction::find('4')->setStatus('Pending');

        PointsTransaction::create([
            'user_id' => '2',
            'transaction_type' => 'Incoming',
            'transfer_to' => null,
            'transfer_from' => User::find(3)->name,
            'completed_at' => null,
            'description' => 'Payment for the service - pending for approval',
            'offer_id' => 2,
            'amount' => 20,
        ]);
        PointsTransaction::find('4')->setStatus('Pending');

        PointsTransaction::create([
            'user_id' => '2',
            'transaction_type' => 'Outgoing',
            'transfer_to' => User::find(1)->name,
            'transfer_from' => null,
            'completed_at' => null,
            'description' => 'Payment for the item - something went wrong',
            'offer_id' => 3,
            'amount' => 20,
        ]);
        PointsTransaction::find('5')->setStatus('Error');

        PointsTransaction::create([
            'user_id' => '4',
            'transaction_type' => 'Incoming',
            'transfer_to' => null,
            'transfer_from' => null,
            'completed_at' => now(),
            'description' => 'Points from subscription',
            'offer_id' => null,
            'amount' => 50,
            'subscription' => 1
        ]);
        PointsTransaction::find('6')->setStatus('Success');

        PointsTransaction::create([
            'user_id' => '3',
            'transaction_type' => 'Incoming',
            'transfer_to' => null,
            'transfer_from' => null,
            'completed_at' => now(),
            'description' => 'Points from subscription',
            'offer_id' => null,
            'amount' => 50,
            'subscription' => 1
        ]);
        PointsTransaction::find('7')->setStatus('Success');

        PointsTransaction::create([
            'user_id' => '2',
            'transaction_type' => 'Incoming',
            'transfer_to' => null,
            'transfer_from' => null,
            'completed_at' => now(),
            'description' => 'Points from subscription',
            'offer_id' => null,
            'amount' => 50,
            'subscription' => 1
        ]);
        PointsTransaction::find('8')->setStatus('Error');
    }
}

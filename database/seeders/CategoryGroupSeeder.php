<?php

namespace Database\Seeders;

use App\Models\CategoryGroup;
use Illuminate\Database\Seeder;

class CategoryGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat1 = CategoryGroup::create(['name' => 'Nieruchomości']);
        $cat1->addMediaFromUrl('https://barter-test.s3-eu-west-1.amazonaws.com/icons/house.png')->toMediaCollection('category_group_avatars');

        $cat2 = CategoryGroup::create(['name' => 'Motoryzacja']);
        $cat2->addMediaFromUrl('https://barter-test.s3-eu-west-1.amazonaws.com/icons/motorization.png')->toMediaCollection('category_group_avatars');

        $cat3 = CategoryGroup::create(['name' => 'Usługi']);
        $cat3->addMediaFromUrl('https://barter-test.s3-eu-west-1.amazonaws.com/icons/service.png')->toMediaCollection('category_group_avatars');

        $cat4 = CategoryGroup::create(['name' => 'Praca']);
        $cat4->addMediaFromUrl('https://barter-test.s3-eu-west-1.amazonaws.com/icons/job.png')->toMediaCollection('category_group_avatars');

        $cat5 = CategoryGroup::create(['name' => 'Moda']);
        $cat5->addMediaFromUrl('https://barter-test.s3-eu-west-1.amazonaws.com/icons/clothes.png')->toMediaCollection('category_group_avatars');

        $cat6 = CategoryGroup::create(['name' => 'Elektronika']);
        $cat6->addMediaFromUrl('https://barter-test.s3-eu-west-1.amazonaws.com/icons/electronic.png')->toMediaCollection('category_group_avatars');

        $cat7 = CategoryGroup::create(['name' => 'Zwierzęta']);
        $cat7->addMediaFromUrl('https://barter-test.s3-eu-west-1.amazonaws.com/icons/pets.png')->toMediaCollection('category_group_avatars');
    }
}

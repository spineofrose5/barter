<?php

namespace Database\Seeders;

use App\Models\CategoryGroup;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesSeeder::class,
            UsersSeeder::class,
            ProfileSeeder::class,
            CategoryGroupSeeder::class,
            CategorySeeder::class,
            OfferSeeder::class,
            TransactionSeeder::class,
            SubscriptionSeeder::class,
        ]);
    }
}

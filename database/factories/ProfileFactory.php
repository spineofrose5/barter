<?php

namespace Database\Factories;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Profile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        User::unsetEventDispatcher();

        return [
            'first_name' => $this->faker->firstName,
            'surname' => $this->faker->lastName,
            'country' => $this->faker->country,
            'city' => $this->faker->city,
            'postal_code' => $this->faker->postcode,
            'street' => $this->faker->streetName,
            'number' => $this->faker->buildingNumber,
            'contact_phone' => $this->faker->phoneNumber,
            'points' => $this->faker->numberBetween(0, 200),
            'user_id' => $this->faker->unique()->numberBetween(1, 11)
        ];
    }
}

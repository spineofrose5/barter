<?php

namespace Database\Factories;

use App\Models\Offer;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OfferFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Offer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word,
            'description' => $this->faker->randomHtml(),
            'offer_type' => $this->faker->randomElement(['item', 'service', 'advertisement']),
            'payment_type' => $this->faker->randomElement(['free', 'points', 'exchange']),
            'coast' => $this->faker->numberBetween(1, 99),
            'exchange_for' => $this->faker->sentence,
            'user_id' => $this->faker->numberBetween(1, 11),
            'category_id' => $this->faker->numberBetween(1,50),
            'published_at' => $this->faker->dateTime
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (Offer $offer) {
            $offer->setStatus($this->faker->randomElement(['Active', 'Inactive', 'Realized']));
        });
    }
}

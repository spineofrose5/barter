<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'group_id'
    ];

    public function categoryGroup()
    {
        return $this->belongsTo(CategoryGroup::class, 'group_id');
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }
}

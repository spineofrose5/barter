<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\ModelStatus\HasStatuses;

class Offer extends Model implements HasMedia
{
    use HasFactory, SoftDeletes, HasStatuses, InteractsWithMedia;

    /**
     * Model statuses const and static array variable $statuses
     *
     * @var array
     */
    public const ACTIVE = 'Active';
    public const INACTIVE = 'Inactive';
    public const REALIZED = 'Realized';

    public static $statuses_types = [self::ACTIVE, self::INACTIVE, self::REALIZED];

    // Offers types
    CONST ITEM = 'item';
    CONST SERVICE = 'service';
    CONST ADVERTISEMENT = 'advertisement';

    public static $offer_types = [self::ITEM, self::SERVICE, self::ADVERTISEMENT];

    // Payments types
    CONST FREE = 'free';
    CONST POINTS_TYPE = 'points';
    CONST EXCHANGE_TYPE = 'exchange';

    public static $payment_types = [self::FREE, self::POINTS_TYPE, self::EXCHANGE_TYPE];

    protected $table = 'offers';

    protected $fillable = [
        'title',
        'description',
        'category_id',
        'offer_type',
        'payment_type',
        'coast',
        'exchange_for',
        'published_at'
    ];

//    protected $casts = [
//        'published_at' => 'date',
//        'deleted_at' => 'datatime',
//        'created_at' => 'datatime',
//        'updated_at' => 'datatime',
//    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(100)
            ->height(100);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function transactions()
    {
        return $this->hasMany(PointsTransaction::class, 'offer_id');
    }
}

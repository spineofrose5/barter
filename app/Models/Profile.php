<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Profile extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'first_name',
        'surname',
        'country',
        'city',
        'postal_code',
        'street',
        'number',
        'contact_phone',
        'user_id',
        'points'
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(100)
            ->height(100);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getFullName(): string
    {
        return $this->first_name . ' ' . $this->surname;
    }

    public function getFullAddress(): string
    {
        return $this->country . ', ' . $this->city . ' ' . $this->street . ' ' . $this->number;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\ModelStatus\HasStatuses;

class PointsTransaction extends Model
{
    use HasFactory, HasStatuses, SoftDeletes;

    /**
     * Transaction statuses
     */
    PUBLIC CONST PENDING = 'Pending';
    PUBLIC CONST SUCCESS = 'Success';
    PUBLIC CONST ERROR = 'Error';

    public static $pointsTransactionStatuses = [self::PENDING, self::SUCCESS, self::ERROR];

    /**
     * Transaction types
     */
    PUBLIC CONST INCOMING = 'Incoming';
    PUBLIC CONST OUTGOING = 'Outgoing';

    public static $pointsTransactionTypes = [self::INCOMING, self::OUTGOING];

    protected $table = 'points_transaction';

    protected $fillable = [
        'user_id',
        'transaction_type',
        'transfer_to',
        'transfer_from',
        'completed_at',
        'description',
        'offer_id',
        'amount',
        'subscription'
    ];

    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeSubscription($query)
    {
        return $query->where('subscription', 1);
    }
}

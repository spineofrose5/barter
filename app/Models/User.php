<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\ModelStatus\HasStatuses;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes, HasRoles, HasStatuses;

    /**
     * Model statuses const and static array variable $statuses
     *
     * @var array
     */
    public const ACTIVE = 'Active';
    public const INACTIVE = 'Inactive';
    public const DEACTIVATED = 'Deactivated';

    public static $statuses_types = [self::ACTIVE, self::INACTIVE, self::DEACTIVATED];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Only accept a valid password and
     * hash a password before saving
     */
    public function setPasswordAttribute($password)
    {
        if ( $password !== null & $password !== "" )
        {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    public function transactions()
    {
        return $this->hasMany(PointsTransaction::class);
    }
}

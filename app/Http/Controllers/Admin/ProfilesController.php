<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Models\Profile;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProfilesController extends Controller
{

    public function store(Request $request): RedirectResponse
    {
        //
    }

    public function update(ProfileRequest $request, Profile $profile): RedirectResponse
    {
        $profile->update($request->validated());

        return back()->with('success', __('Saved correctly.'));
    }

}

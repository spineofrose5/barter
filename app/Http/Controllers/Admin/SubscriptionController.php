<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function index()
    {
        $subscriptions = Subscription::all();
        return view('admin.subscriptions.index', compact('subscriptions'));
    }

    public function getSubscription(Subscription $subscription)
    {
        $user = auth()->user();
        $points = $user->profile->points;
        $user->profile()->update(['points' => $points += $subscription->points]);

        return back(201)->with('success', __('Pomyślnie kupiłeś abonament. Punkty zostały naliczone'));
    }
}

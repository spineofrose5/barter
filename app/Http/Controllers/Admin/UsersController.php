<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Offer;
use App\Models\PointsTransaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{

    public function index(): View
    {
        $users = User::paginate(10);

        return view('admin.users.index', compact('users'));
    }

    public function show(User $user): View
    {
        $offers = Offer::where('user_id', $user->id)->with('category')->paginate(10);
        $transactions = PointsTransaction::where('user_id', $user->id)->paginate(10);

        return view('admin.users.show', compact('user', 'offers', 'transactions'));
    }

    public function edit(User $user): View
    {
        $statuses = User::$statuses_types;

        $roles = Role::all();

        return view('admin.users.edit', compact('user', 'roles', 'statuses'));
    }

    public function update(UserUpdateRequest $request, User $user): RedirectResponse
    {
        $user->update($request->validated());

        return back()->with('success', __('Saved correctly.'));
    }

    public function destroy(User $user): RedirectResponse
    {
        $user->delete();

        return back()->with('success', __('Deleted correctly.'));
    }

    public function changeRole(User $user, Request $request): RedirectResponse
    {
        $user->roles()->detach();
        $user->roles()->sync($request->get('role'));

        return back(201)->with('success', __('Saved correctly.'));
    }

    public function changeStatus(User $user, Request $request): RedirectResponse
    {
        $user->setStatus($request->get('status'));

        return back(201)->with('success', __('Saved correctly.'));
    }
}

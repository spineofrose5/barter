<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\GroupCategoryRequest;
use App\Models\CategoryGroup;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CategoryGroupController extends Controller
{
    public function index(): View
    {
        $categoryGroups = CategoryGroup::withCount(['categories', 'offers'])->with('media')->paginate(20);

        return view('admin.group_category.index', compact('categoryGroups'));
    }

    public function store(GroupCategoryRequest $request): RedirectResponse
    {
        $categoryGroup = CategoryGroup::create($request->validated());

        $categoryGroup->addMediaFromRequest('avatar')->toMediaCollection('category_group_avatars');

        return back(201)->with('success', __('Saved correctly.'));
    }

    public function show(CategoryGroup $categoryGroup): View
    {
        //
    }

    public function edit(CategoryGroup $categoryGroup): View
    {
        $categories = $categoryGroup->categories()
            ->withCount('offers')
            ->paginate(5);

        return view('admin.group_category.edit', compact( 'categoryGroup', 'categories'));
    }

    public function update(GroupCategoryRequest $request, CategoryGroup $categoryGroup): RedirectResponse
    {
        if ($request->hasFile('avatar'))
        {
            if ($categoryGroup->getFirstMedia('category_group_avatars'))
            {
                $categoryGroup->getFirstMedia('category_group_avatars')->delete();
            }

            $categoryGroup->addMediaFromRequest('avatar')->toMediaCollection('category_group_avatars');
        }
        $categoryGroup->update($request->validated());

        return back(201)->with('success', __('Saved correctly.'));
    }

    public function destroy(CategoryGroup $categoryGroup): RedirectResponse
    {
        $categoryGroup->delete();

        return back(201)->with('success', __('Deleted correctly.'));
    }
}

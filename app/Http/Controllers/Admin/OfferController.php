<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryGroup;
use App\Models\Offer;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\ModelStatus\Status;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $offers = Offer::with(['category', 'user', 'statuses'])->paginate(20);

        return view('admin.offers.index', compact('offers'));
    }

    /**
     * Display the specified resource.
     *
     * @param Offer $offer
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $offer = Offer::with('category', 'user', 'media')->findOrFail($id);
//        dd($offer);
        return view('admin.offers.show', compact('offer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Offer $offer
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $offer = Offer::where('id', $id)->with('media')->first();
        $categories = CategoryGroup::with('categories')->get();
        $statuses = Offer::$statuses_types;

        return view('admin.offers.edit', compact('offer', 'categories', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Offer $offer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Offer $offer)
    {
        if ($request->hasFile('avatar'))
        {
            $offer->addMediaFromRequest('avatar')->toMediaCollection('offer_images');
        }

        $offer->update($request->all());

        return back(201)->with('success', __('Saved correctly.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Offer $offer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Offer $offer)
    {
        $offer->delete();

        return back(201)->with('success', __('Deleted correctly.'));
    }

    public function deleteMedia($media)
    {
        if (! Media::where('id', $media)->first()){
            return back()->with('error', __('dashboard.global.errors.dont_find'));
        } else {
            Media::where('id', $media)->delete();
            return back(201)->with('success', __('Deleted correctly.'));
        }
    }
}

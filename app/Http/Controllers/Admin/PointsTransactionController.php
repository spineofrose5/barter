<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use App\Models\PointsTransaction;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;


class PointsTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $pointsTransactions = PointsTransaction::with('statuses', 'offer')->where('subscription', 0)->latest()->paginate(20);
        $subscriptions = PointsTransaction::subscription()->latest()->paginate(20);

        return view('admin.points_transactions.index', compact('pointsTransactions', 'subscriptions'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * Soft delete
     *
     * @param PointsTransaction $pointTransaction
     * @return RedirectResponse
     */
    public function destroy(PointsTransaction $pointTransaction): RedirectResponse
    {
        $pointTransaction->delete();

        return back(201)->with('success', __('Deleted correctly.'));
    }

    public function transfer($id)
    {
        $offer = Offer::with('user')->findOrFail($id);
        $authUserPoints = auth()->user()->profile->points;
        $userPoints = $offer->user->profile->points;

        if ($offer->coast > $authUserPoints)
            return back()->with('error', 'Nie masz tylu punktów na koncie');

        auth()->user()->profile->update(['points' => $authUserPoints - $offer->coast]);

        Profile::unsetEventDispatcher();
        $offer->user->profile->update(['points' => $userPoints + $offer->coast]);

        PointsTransaction::create([
            'user_id' => auth()->id(),
            'transaction_type' => PointsTransaction::OUTGOING,
            'transfer_to' => $offer->user->id,
            'transfer_from' => auth()->id(),
            'completed_at' => now(),
            'description' => 'Payment for the item',
            'amount' => $offer->coast,
            'offer_id' => $offer->id
        ])->setStatus(PointsTransaction::SUCCESS);

        return back()->with('success', 'Wszystko poszło ok :)');
    }
}

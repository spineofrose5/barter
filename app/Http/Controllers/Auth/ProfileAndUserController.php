<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\AuthPasswordEdit;
use App\Http\Requests\ProfileRequest;
use App\Models\Profile;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;


class ProfileAndUserController extends Controller
{
    public function create(): view
    {
        return view('auth.profiles.create');
    }
    public function store(ProfileRequest $request)
    {
        $profile = Profile::updateOrCreate(
            ['user_id' => auth()->id()],
            $request->validated()
        );

        if ($request->hasFile('avatar')) {
            $profile->addMediaFromRequest('avatar')->toMediaCollection('user_avatars');
        }

        return back(201)->with('success', __('Saved correctly.'));
    }

    public function updateProfile(ProfileRequest $request): RedirectResponse
    {
        if ($request->hasFile('avatar')){
            if (auth()->user()->profile->getFirstMedia('user_avatars'))
            {
                auth()->user()->profile->getFirstMedia('user_avatars')->delete();
            }

            auth()->user()->profile->addMediaFromRequest('avatar')->toMediaCollection('user_avatars');
        }
        auth()->user()->profile->update($request->validated());

        return redirect()->back(201)->with('success', __('Saved correctly.'));
    }

    public function updateLogin(UserUpdateRequest $request)
    {
        auth()->user()->update($request->validated());

        return back()->with('success', __('Saved correctly.'));
    }

    public function updatePassword(AuthPasswordEdit $request)
    {
        auth()->user()->update($request->validated());

        return back(201)->with('success', __('Saved correctly.'));
    }
}

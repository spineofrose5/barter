<?php

namespace App\Http\Middleware;

use App\Models\Profile;
use Closure;
use Illuminate\Http\Request;

class HasProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (! Profile::where('user_id', '=', auth()->id())->first()) {
            return redirect()->route('auth.profile.create')
                ->with('error', __('dashboard.global.no_profile_message'));
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GroupCategoryRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'min:3',
                'max:20',
                Rule::unique('category_groups', 'name')->ignore($this->category_group)
            ],
            'avatar' => [
                'sometimes',
                'file',
                'max:1024',
                'mimes:jpg,jpeg,png,svg'
            ]
        ];
    }
}

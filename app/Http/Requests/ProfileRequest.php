<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'first_name' => 'required|string|min:3|max:20',
            'surname' => 'required|string|min:3|max:20',
            'country' => 'sometimes|string|min:3|max:20',
            'city' => 'sometimes|string|min:3|max:20',
            'postal_code' => 'sometimes|string|min:3|max:10',
            'street' => 'sometimes|string|min:3|max:30',
            'number' => 'sometimes|min:1|max:5',
            'contact_phone' => [
                'required',
                Rule::unique('profiles', 'contact_phone')->ignore($this->profile)
            ],
            'avatar' => [
                'sometimes',
                'file',
                'max:1024',
                'mimes:jpg,jpeg,png,svg'
            ]
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'min:3',
                'max:20'
            ],
            'email' => [
                'email',
                'required',
                Rule::unique('users', 'email')->ignore($this->user)
            ]
        ];
    }
}

<?php

namespace Tests\Feature;

use App\Models\CategoryGroup;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class CategoryGroupTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('s3');

        Role::create(['name' => 'Admin']);
        Role::create(['name' => 'User']);

        $admin = User::factory()->create();
        Profile::factory()->for($admin)->create();
        $admin->assignRole('Admin');

        $this->actingAs($admin);
    }

    /**
     * Only admin can manage category groups
     *
     * @return void
     */
    public function testNoAdminUserCantSeeCategoryGroups()
    {
        $user = User::factory()->create();
        $user->assignRole('User');

        $this->actingAs($user)->get('/admin/category_groups')
            ->assertStatus(403);
    }

    /**
     *  admin is actingAS in whole test (setUp func)
     */
    public function testAdminUserCanManageCategoryGroups()
    {
        $categoryGroup = CategoryGroup::factory()->create(['name' => 'testowa kategoria']);

        $this->get('admin/category_groups')
            ->assertStatus(200);

        // admin user can create new category_group
        $response = $this->post('admin/category_groups', [
            'name' => 'Wyroby szklane',
            'avatar' => UploadedFile::fake()->image('photo.jpeg', 500, 500)->size(1000)
        ]);
        $response->assertStatus(201);

        $created_group = CategoryGroup::where('name', 'Wyroby szklane')->first();
        $this->assertNotNull($created_group);

        // admin can update category_group
        $response = $this->put('admin/category_groups/'.$categoryGroup->id, [
            'name' => 'Nowa nazwa',
            'avatar' => UploadedFile::fake()->image('photo.jpeg', 500, 500)->size(1000)
        ]);
        $response->assertStatus(201);

        $updated_group = CategoryGroup::where('name', 'Nowa nazwa')->first();
        $this->assertNotNull($updated_group);

        // admin can delete category
        $response = $this->delete('admin/category_groups/'.$categoryGroup->id);
        $response->assertStatus(201);

        $deleted_group = CategoryGroup::where('name', 'Nowa nazwa')->first();
        $this->assertNull($deleted_group);
    }

    public function testCategoryAvatarShowCorrectly()
    {
        $response = $this->post('admin/category_groups', [
            'name' => 'Wyroby szklane',
            'avatar' => UploadedFile::fake()->image('photo.jpeg', 500, 500)->size(1000)
        ]);
        $response->assertStatus(201);

        $this->get('admin/category_groups')->assertSee('avatar');
    }
}

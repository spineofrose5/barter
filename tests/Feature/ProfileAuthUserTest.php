<?php

namespace Tests\Feature;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class ProfileAuthUserTest extends TestCase
{
    use RefreshDatabase;

    private $profile;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('s3');

        Role::create(['name' => 'User']);

        $user = User::factory()->create();
        $this->profile = Profile::factory()->for($user)->create();
        $user->assignRole('User');
        $this->actingAs($user);
    }

    /**
     * user can see edit profile form
     *
     * @return void
     */
    public function testUserCanSeeOwnProfilePage()
    {
        $response = $this->get('/auth/profile/edit');

        $response->assertSuccessful();
    }

    public function testUserCanCreateNewOneProfile()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post(route('auth.profile.store'), [
            'first_name' => 'test',
            'surname' => 'nazwisko',
            'country' => 'polska',
            'city' => 'opole',
            'postal_code' => '45-056',
            'street' => 'woźniaka',
            'number' => '12121',
            'contact_phone' => '789789789',
            'avatar' => UploadedFile::fake()->image('photo.jpeg', 500, 500)->size(1000)
        ]);
        $response->assertStatus(201);
    }

    public function testUserCanEditProfile()
    {
        $response = $this->put('/auth/profile/'.$this->profile.'/update', [
            'first_name' => 'sadadas',
            'surname' => 'asdasdasd',
            'country' => 'asdsads',
            'city' => 'sdsds',
            'postal_code' => 'asdasdas',
            'street' => 'sadsadsa',
            'number' => '12121',
            'contact_phone' => '789789789',
            'avatar' => UploadedFile::fake()->image('photo.jpeg', 500, 500)->size(1000)
        ]);

        $response->assertStatus(201);
    }

    /**
     * test redirect to create profile page when user go to another URI
     */
    public function testUserWithoutProfileMustCreateOne()
    {
        $user = User::factory()->create();

        $this->actingAs($user)->get('/auth/profile/edit')
            ->assertRedirect('/auth/profile/create');
    }


    public function testUserCanChangePassword()
    {
        $user = User::factory()->create(['password' => 'password']);
        Profile::factory()->for($user)->create();

        $this->actingAs($user)->put('auth/password/'.$user->id.'/update', [
            'current_password' => 'password',
            'password' => 'newPassword',
            'password_confirmation' => 'newPassword'
        ])->assertStatus(201);
    }
}

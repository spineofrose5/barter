<div class="modal fade" tabindex="-1" role="dialog" id="deleteUserConfirmModal">
    <form id="delete-form" action="{{route('admin.users.destroy', $user)}}" method="post">
        @csrf
        @method('DELETE')
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <strong>{{__('dashboard.users.index.confirmation_delete_user')}}</strong>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">{{__('dashboard.buttons.modals.modal_delete_submit_button')}}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('dashboard.buttons.modals.modal_delete_cancel_button')}}</button>
                </div>
            </div>
        </div>
    </form>
</div>

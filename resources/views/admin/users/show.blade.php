@extends('layouts.app')
@section('content')
    <div class="col-md-12">
        <div class="card text-center">
            <div class="card-header">
                {{__('dashboard.global.role')}}: <strong>{{$user->roles->pluck('name')->implode(' ')}}</strong>
            </div>
            <div class="card-body">
                <h5 class="card-title">{{$user->profile->getFullName()}}</h5>
                <p class="card-text">{{$user->profile->getFullAddress()}}</p>
                <p class="card-text">{{$user->profile->contact_phone}}</p>
                {{--                    <a href="#" class="btn btn-primary">Go somewhere</a>--}}
            </div>
            <div class="card-footer text-muted">
                {{$user->email}}
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card card-body text-center">
            <strong>Ilość punktów użytkownika: {{$user->profile->points}}</strong>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card text-center">
            <div class="card-body">
                <form action="{{route('points.transfer', $user)}}" method="post">
                    @csrf
                    @method('POST')
                    <strong>Przelej punkty do użytkownika: {{$user->name}}</strong>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="points">Ilość</label>
                                <input type="number" name="points" id="points" class="form-control" value="{{old('points')}}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-outline-success">Prześlij</button>
                        </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><i
                    class="fa fa-align-justify"></i><strong> {{__('dashboard.global.user_offers')}}</strong></div>
            @if($offers->isEmpty())
                <p>{{__('dashboard.global.is_empty')}}</p>
            @else
                <div class="card-body">
                    <table class="table table-responsive-sm">
                        <thead>
                        <tr>
                            <th>{{__('dashboard.global.title')}}</th>
                            <th>{{__('dashboard.global.offer_type')}}</th>
                            <th>{{__('dashboard.global.category_name')}}</th>
                            {{--                            <th>{{__('dashboard.global.quantity')}}</th>--}}
                            <th>{{__('dashboard.global.coast')}}</th>
                            <th>{{__('dashboard.global.payment_type')}}</th>
                            <th>{{__('dashboard.global.status')}}</th>
                            <th>{{__('dashboard.global.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($offers as $offer)
                            <tr>
                                <td>{{ $offer->title }}</td>
                                <td>{{ $offer->offer_type }}</td>
                                <td><strong>{{ $offer->category->categoryGroup->name }}</strong>
                                    > {{ $offer->category->name }}</td>
                                <td>{{ $offer->coast }}</td>
                                <td>
                                    {{ $offer->payment_type }}<br>
                                    @if($offer->payment_type == 'exchange')
                                        <small>{{ $offer->exchange_for }}</small>
                                    @endif
                                </td>
                                <td><span class="badge badge-success">{{ $offer->status }}</span></td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{route('admin.users.show', $user)}}">
                                            <svg class="btn btn-outline btn-light"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use
                                                    xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-info')}}"></use>
                                            </svg>
                                        </a>
                                        <a href="{{route('admin.users.edit', $user)}}">
                                            <svg class="btn btn-outline btn-info"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use
                                                    xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
                                            </svg>
                                        </a>
                                        <button type="button" class="btn btn-primary"
                                                style="padding: 0; border: 0; display: inline"
                                                data-toggle="modal"
                                                data-target="#deleteUserConfirmModal">
                                            <svg class="btn btn-outline btn-danger"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use
                                                    xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-trash')}}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination">{{ $offers->links() }}</div>
                </div>
            @endif
        </div>
    </div>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><i class="fa fa-align-justify"></i>
                <strong>{{__('dashboard.points_transactions.index.title')}}</strong></div>
            @if($transactions->isEmpty())
                <p>{{__('dashboard.global.is_empty')}}</p>
            @else
                <div class="card-body">
                    <table class="table table-responsive-sm">
                        <thead>
                        <tr>
                            <th>{{__('dashboard.global.username')}}</th>
                            <th>{{__('dashboard.points_transactions.index.transaction_type')}}</th>
                            <th>{{__('dashboard.points_transactions.index.created_at')}}</th>
                            <th>{{__('dashboard.points_transactions.index.amount')}}</th>
                            <th>{{__('dashboard.global.status')}}</th>
                            <th>{{__('dashboard.points_transactions.index.completed_at')}}</th>
                            <th>{{__('dashboard.global.description')}}</th>
                            <th>{{__('dashboard.global.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transactions as $transaction)
                            <tr>
                                <td>{{ $transaction->user->name }}</td>
                                <td>{{ $transaction->transaction_type }}</td>
                                <td>{{ $transaction->created_at }}</td>
                                <td>{{ $transaction->amount }}</td>
                                <td><span class="badge badge-success">{{ $transaction->status }}</span></td>
                                <td>{{ $transaction->completed_at }}</td>
                                <td>{{ $transaction->description }}</td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{route('admin.pointsTransactions.edit', $transaction)}}">
                                            <svg class="btn btn-outline btn-info"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use
                                                    xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
                                            </svg>
                                        </a>
                                        <button type="button" class="btn btn-primary"
                                                style="padding: 0; border: 0; display: inline"
                                                data-toggle="modal"
                                                data-target="#deleteTransactionConfirmModal">
                                            <svg class="btn btn-outline btn-danger"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use
                                                    xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-trash')}}"></use>
                                            </svg>
                                        </button>
                                        @include('admin.users.modals.deleteTransactionConfirmModal')
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination">{{ $transactions->links() }}</div>
                </div>
            @endif
        </div>
    </div>
@endsection

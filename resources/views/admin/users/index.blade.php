@extends('layouts.app')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><i class="fa fa-align-justify"></i> <strong>{{__('dashboard.global.users')}}</strong></div>
            <div class="card-body">
                <table class="table table-responsive-sm">
                    <thead>
                    <tr>
                        <th>{{__('dashboard.global.username')}}</th>
                        <th>{{__('dashboard.global.email_address')}}</th>
                        <th>{{__('dashboard.global.name')}}</th>
                        <th>{{__('dashboard.global.address')}}</th>
                        <th>{{__('dashboard.global.points')}}</th>
                        <th>{{__('dashboard.global.role')}}</th>
                        <th>{{__('dashboard.global.offers_count')}}</th>
                        <th>{{__('dashboard.global.status')}}</th>
                        <th>{{__('dashboard.global.actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->profile->getFullName() }}</td>
                            <td>{{ $user->profile->getFullAddress() }}</td>
                            <td>{{ $user->profile->points }}</td>
                            <td>{{ $user->roles->pluck('name')->implode(' ') }}</td>
                            <td>5</td>
                            <td><span class="badge badge-success">{{ $user->status }}</span></td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="{{route('admin.users.show', $user)}}">
                                        <svg class="btn btn-outline btn-light"
                                             style="max-height: 25px; max-width: 25px; padding: 3px">
                                            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-info')}}"></use>
                                        </svg>
                                    </a>
                                    <a href="{{route('admin.users.edit', $user)}}">
                                        <svg class="btn btn-outline btn-info"
                                             style="max-height: 25px; max-width: 25px; padding: 3px">
                                            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
                                        </svg>
                                    </a>
                                    <button type="button" class="btn btn-primary"
                                            style="padding: 0; border: 0; display: inline"
                                            data-toggle="modal"
                                            data-target="#deleteUserConfirmModal">
                                        <svg class="btn btn-outline btn-danger"
                                             style="max-height: 25px; max-width: 25px; padding: 3px">
                                            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-trash')}}"></use>
                                        </svg>
                                    </button>
                                    @include('admin.users.modals.deleteUserConfirm')
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination">{{ $users->links() }}</div>
            </div>
        </div>
    </div>
@endsection

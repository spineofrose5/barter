@extends('layouts.app')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                <strong>{{__('dashboard.offers.index.table_title')}}</strong>
            </div>
            <div class="card-body">
                @if(! $offers)
                    <div class="card-body">
                        {{__('dashboard.global.is_empty')}}
                    </div>
                @else
                    <table class="table table-responsive-sm">
                        <thead>
                        <tr>
                            <th>{{__('dashboard.global.title')}}</th>
                            <th>{{__('dashboard.global.offer_type')}}</th>
                            <th>{{__('dashboard.global.category_name')}}</th>
                            {{--                            <th>{{__('dashboard.global.quantity')}}</th>--}}
                            <th>{{__('dashboard.global.coast')}}</th>
                            <th>{{__('dashboard.global.payment_type')}}</th>
                            <th>{{__('dashboard.global.status')}}</th>
                            <th>{{__('dashboard.global.username')}}</th>
                            <th>{{__('dashboard.global.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($offers as $offer)
                            <tr>
{{--                                <td>--}}
{{--                                    @if(! $offer->getFirstMedia('offer_images'))--}}
{{--                                        <img src="https://via.placeholder.com/100" style="max-width: 300px" alt="">--}}
{{--                                    @else--}}
{{--                                        <img src="{{$offer->getFirstMediaUrl('offer_images', 'thumb')}}"--}}
{{--                                             style="max-height: 100px" alt="">--}}
{{--                                    @endif--}}
{{--                                </td>--}}
                                <td>{{ $offer->title }}</td>
                                <td>{{ $offer->offer_type }}</td>
                                <td><strong>{{ $offer->category->categoryGroup->name }}</strong> > {{ $offer->category->name }}</td>
                                <td>{{ $offer->coast }}</td>
                                <td>
                                    {{ $offer->payment_type }}<br>
                                    @if($offer->payment_type == 'exchange')
                                        <small>{{ $offer->exchange_for }}</small>
                                    @endif
                                </td>
                                <td><span class="badge badge-success">{{ $offer->status }}</span></td>
                                <td><a href="{{route('admin.users.show', $offer->user->id)}}">{{ $offer->user->name }}</a></td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{route('admin.offers.show', $offer)}}">
                                            <svg class="btn btn-outline btn-light"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-info')}}"></use>
                                            </svg>
                                        </a>
                                        <a href="{{route('admin.offers.edit', $offer)}}">
                                            <svg class="btn btn-outline btn-info"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
                                            </svg>
                                        </a>
                                        <button type="button" class="btn btn-primary"
                                                style="padding: 0; border: 0; display: inline"
                                                data-toggle="modal"
                                                data-target="#deleteOfferConfirmModal">
                                            <svg class="btn btn-outline btn-danger"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-trash')}}"></use>
                                            </svg>
                                        </button>
                                        @include('admin.offers.modals.deleteOfferConfirmModal')
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination">{{ $offers->links() }}</div>
                @endif
            </div>
        </div>
    </div>

@endsection

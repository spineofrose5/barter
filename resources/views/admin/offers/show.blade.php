@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h1>{{ $offer->title }}</h1><br>
                    <p>{{__('dashboard.global.created_at')}}: {{ $offer->created_at }},
                        {{__('dashboard.global.in_category')}}: {{ $offer->category->name }}</p>
                    <p>
                        {{__('dashboard.global.created_by_user')}}:
                        <a href="{{route('admin.users.show', $offer->user->id)}}">{{ $offer->user->name }}</a>
                    </p>
                    <p>{{__('dashboard.global.offer_type')}}: {{ $offer->offer_type }}</p>

                </div>
                <div class="card-body">
                    <h3>{!! $offer->description !!}</h3> <br>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-body">
                <h3>{{__('dashboard.global.photos')}}</h3>
                <div class="d-inline">
                    @foreach($offer->getMedia('offer_images') as $media)
                        <img src="{{$media->getUrl('thumb')}}" alt="" height="100px" width="100px" class="img-fluid m-1">
                    @endforeach
                </div>
            </div>

            @if($offer->payment_type == \App\Models\Offer::POINTS_TYPE)
                <div class="card card-body">
                        <p>{{__('dashboard.global.coast')}}: <strong>{{ $offer->coast }}</strong></p>
                        <form action="{{route('points.transfer', $offer)}}" method="post">
                            @csrf
                            @method('POST')
{{--                            <input type="hidden" name="points" id="points" value="{{$offer->coast}}">--}}
                            <div class="form-group">
                                <button type="submit" class="btn btn-dark"
                                        @if($offer->coast > auth()->user()->profile->points) disabled @endif>
                                        {{ ($offer->coast > auth()->user()->profile->points) ? __('dashboard.global.dont_enough_points') : __('dashboard.global.buy') }}
                                </button>
                            </div>
                        </form>
                </div>
            @endif
        </div>
    </div>
@endsection

@extends('layouts.app')
@section('content')
    <style>
        .ck.ck-content {
            /* Set vertical boundaries for the document editor. */
            height: 400px;
            overflow: auto;
        }
    </style>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><strong>{{__('dashboard.offers.edit.title')}}</strong></div>
            <div class="card-body">
                <form class="form-horizontal" action="{{route('admin.offers.update', $offer)}}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="title">{{__('dashboard.offers.edit.name')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="title" type="text" name="title" autocomplete="title"
                                   value="{{old('title', $offer->title)}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="category">{{__('dashboard.global.category_name')}}</label>
                        <div class="col-md-9">
                            <select name="category" id="category" class="form-control">
                                @foreach($categories as $group)
                                    <option disabled value=""><strong>{{$group->name}}</strong></option>
                                    @foreach($group->categories as $category)
                                        <option value="{{$category->id}}"> ---{{$category->name}}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="offer_type">{{__('dashboard.global.offer_type')}}</label>
                        <div class="col-md-9">
                            <select name="offer_type" id="offer_type" class="form-control">
                                @foreach($offer::$offer_types as $offer_type)
                                    <option {{ ($offer->offer_type == $offer_type ? 'selected' : '')}}
                                        value="{{$offer_type}}">
                                        {{$offer_type}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="payment_type">{{__('dashboard.global.payment_type')}}</label>
                        <div class="col-md-9">
                            <select name="payment_type" id="payment_type" class="form-control">
                                @foreach($offer::$payment_types as $payment_type)
                                    <option {{ ($offer->payment_type == $payment_type ? 'selected' : '')}}
                                        value="{{$payment_type}}">
                                        {{$payment_type}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="coast">{{__('dashboard.global.coast')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" type="number" id="coast" name="coast" autocomplete="coast"
                                   value="{{old('coast', $offer->coast)}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="exchange_for">{{__('dashboard.global.exchange_for')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" id="exchange_for" name="exchange_for" autocomplete="exchange_for"
                                   value="{{old('exchange_for', $offer->exchange_for)}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="status">{{__('dashboard.offers.edit.change_status_label')}}</label>
                        <div class="col-md-9">
                            <select name="status" id="status" class="form-control">
                                @foreach($statuses as $status)
                                    <option
                                        {{ ($offer->status == $status ? 'selected' : '')}}
                                        value="{{$status}}">
                                        {{$status}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="published_at">{{__('dashboard.offers.edit.published_at')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" type="date" id="published_at" name="published_at" autocomplete="published_at"
                                value="{{old('published_at', $offer->published_at)}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="description">{{__('dashboard.offers.edit.description')}}</label>
                        <div class="col-md-9">
                            <textarea class="form-control" id="description" name="description">
                                {{old('description', $offer->description)}}
                            </textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">{{__('dashboard.buttons.save_changes')}}</button>
                    </div>
                </form>
                <div class="row">
                    @foreach($offer->getMedia('offer_images') as $media)
                        <div class="card col-2">
                            <div class="card-header">
                                <img src="{{$media->getFullUrl()}}" class="img-fluid" alt="{{ $offer->title }}">
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-danger"
                                        data-toggle="modal"
                                        data-target="#deleteMediaConfirmModal">{{__('dashboard.buttons.delete_button')}}
                                </button>
                                @include('admin.offers.modals.deleteMediaConfirmModal')
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/28.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#description' ), {
                toolbar: [ 'heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote' ],
                heading: {
                    options: [
                        { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                        { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                        { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
                    ]
                }
            } )
            .catch( error => {
                console.log( error );
            } );
    </script>
@endpush

@extends('layouts.app')
@section('content')
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header">
                    {{__('dashboard.global.role')}}: <strong>{{$user->roles->pluck('name')->implode(' ')}}</strong>
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{$user->profile->getFullName()}}</h5>
                    <p class="card-text">{{$user->profile->getFullAddress()}}</p>
                    <p class="card-text">{{$user->profile->contact_phone}}</p>
                    {{--                    <a href="#" class="btn btn-primary">Go somewhere</a>--}}
                </div>
                <div class="card-footer text-muted">
                    {{$user->email}}
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-align-justify"></i> {{__('dashboard.global.user_offers')}}</div>
                <div class="card-body">
                    <table class="table table-responsive-sm">
                        <thead>
                        <tr>
                            <th>{{__('dashboard.global.title')}}</th>
                            <th>{{__('dashboard.global.offer_type')}}</th>
                            <th>{{__('dashboard.global.category_name')}}</th>
{{--                            <th>{{__('dashboard.global.quantity')}}</th>--}}
                            <th>{{__('dashboard.global.coast')}}</th>
                            <th>{{__('dashboard.global.payment_type')}}</th>
                            <th>{{__('dashboard.global.status')}}</th>
                            <th>{{__('dashboard.global.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($offers as $offer)
                            <tr>
                                <td>{{ $offer->title }}</td>
                                <td>{{ $offer->offer_type }}</td>
                                <td><strong>{{ $offer->category->categoryGroup->name }}</strong> > {{ $offer->category->name }}</td>
                                <td>{{ $offer->coast }}</td>
                                <td>
                                    {{ $offer->payment_type }}<br>
                                    @if($offer->payment_type == 'exchange')
                                        <small>{{ $offer->exchange_for }}</small>
                                    @endif
                                </td>
                                <td><span class="badge badge-success">{{ $offer->status }}</span></td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{route('admin.users.show', $user)}}">
                                            <svg class="btn btn-outline btn-light"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-info')}}"></use>
                                            </svg>
                                        </a>
                                        <a href="{{route('admin.users.edit', $user)}}">
                                            <svg class="btn btn-outline btn-info"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
                                            </svg>
                                        </a>
                                        <button type="button" class="btn btn-primary"
                                                style="padding: 0; border: 0; display: inline"
                                                data-toggle="modal"
                                                data-target="#deleteUserConfirmModal">
                                            <svg class="btn btn-outline btn-danger"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-trash')}}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination">{{ $offers->links() }}</div>
                </div>
            </div>
        </div>
@endsection

@extends('layouts.app')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><i class="fa fa-align-justify"></i> <strong>{{__('dashboard.points_transactions.index.title')}}</strong></div>
            <div class="card-body">
                <table class="table table-responsive-sm">
                    <thead>
                    <tr>
                        <th>{{__('dashboard.global.username')}}</th>
                        <th>{{__('dashboard.points_transactions.index.transaction_type')}}</th>
                        <th>{{__('dashboard.points_transactions.index.created_at')}}</th>
                        <th>{{__('dashboard.points_transactions.index.amount')}}</th>
                        <th>{{__('dashboard.points_transactions.index.user_from')}}</th>
                        <th>{{__('dashboard.points_transactions.index.user_to')}}</th>
                        <th>{{__('dashboard.global.status')}}</th>
                        <th>{{__('dashboard.points_transactions.index.completed_at')}}</th>
                        <th>{{__('dashboard.global.description')}}</th>
                        <th>{{__('dashboard.global.offer')}}</th>
                        <th>{{__('dashboard.global.actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pointsTransactions as $subscription)
                        <tr>
                            <td>{{ $subscription->user->name }}</td>
                            <td>{{ $subscription->transaction_type }}</td>
                            <td>{{ $subscription->created_at }}</td>
                            <td>{{ $subscription->amount }}</td>
                            <td>{{ $subscription->transfer_from }}</td>
                            <td>{{ $subscription->transfer_to }}</td>
                            <td><span class="badge badge-success">{{ $subscription->status }}</span></td>
                            <td>{{ $subscription->completed_at }}</td>
                            <td>{{ $subscription->description }}</td>
                            <td>
                                @if(! $subscription->offer)
                                    <p style="color: red;">{{__('dashboard.global.subscription')}}</p>
                                @else
                                    <a href="{{route('admin.offers.show', $subscription->offer)}}">
                                        {{ $subscription->offer->title }}
                                    </a>
                                @endif
                            </td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="{{route('admin.pointsTransactions.edit', $subscription)}}">
                                        <svg class="btn btn-outline btn-info"
                                             style="max-height: 25px; max-width: 25px; padding: 3px">
                                            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
                                        </svg>
                                    </a>
                                    <button type="button" class="btn btn-primary"
                                            style="padding: 0; border: 0; display: inline"
                                            data-toggle="modal"
                                            data-target="#deleteTransactionConfirmModal">
                                        <svg class="btn btn-outline btn-danger"
                                             style="max-height: 25px; max-width: 25px; padding: 3px">
                                            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-trash')}}"></use>
                                        </svg>
                                    </button>
                                    @include('admin.points_transactions.modals.deleteTransactionConfirmModal')
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination">{{ $pointsTransactions->links() }}</div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><i class="fa fa-align-justify"></i> <strong>{{__('dashboard.points_transactions.index.title_second_table')}}</strong></div>
            <div class="card-body">
                <table class="table table-responsive-sm">
                    <thead>
                    <tr>
                        <th>{{__('dashboard.global.username')}}</th>
                        <th>{{__('dashboard.points_transactions.index.transaction_type')}}</th>
                        <th>{{__('dashboard.points_transactions.index.created_at')}}</th>
                        <th>{{__('dashboard.points_transactions.index.amount')}}</th>
                        <th>{{__('dashboard.global.status')}}</th>
                        <th>{{__('dashboard.points_transactions.index.completed_at')}}</th>
                        <th>{{__('dashboard.global.description')}}</th>
                        <th>{{__('dashboard.global.actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($subscriptions as $subscription)
                        <tr>
                            <td>{{ $subscription->user->name }}</td>
                            <td>{{ $subscription->transaction_type }}</td>
                            <td>{{ $subscription->created_at }}</td>
                            <td>{{ $subscription->amount }}</td>
                            <td><span class="badge badge-success">{{ $subscription->status }}</span></td>
                            <td>{{ $subscription->completed_at }}</td>
                            <td>{{ $subscription->description }}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="{{route('admin.pointsTransactions.edit', $subscription)}}">
                                        <svg class="btn btn-outline btn-info"
                                             style="max-height: 25px; max-width: 25px; padding: 3px">
                                            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
                                        </svg>
                                    </a>
                                    <button type="button" class="btn btn-primary"
                                            style="padding: 0; border: 0; display: inline"
                                            data-toggle="modal"
                                            data-target="#deleteTransactionConfirmModal">
                                        <svg class="btn btn-outline btn-danger"
                                             style="max-height: 25px; max-width: 25px; padding: 3px">
                                            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-trash')}}"></use>
                                        </svg>
                                    </button>
                                    @include('admin.points_transactions.modals.deleteTransactionConfirmModal')
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination">{{ $pointsTransactions->links() }}</div>
            </div>
        </div>
    </div>
@endsection

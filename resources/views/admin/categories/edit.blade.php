@extends('layouts.app')
@section('content')
    <div class="col-md-6">
        <div class="card">
            <div class="card-header"><strong>{{__('dashboard.categories.edit.title')}}: {{$category->name}}</strong></div>
            <div class="card-body">
                <form class="form-horizontal" action="{{route('admin.categories.update', $category)}}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="name">{{__('dashboard.group_category.edit.name')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="name" type="text" name="name" autocomplete="name"
                                   value="{{old('name', $category->name)}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="avatar">{{__('dashboard.group_category.edit.avatar')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="avatar" type="file" name="avatar">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">{{__('dashboard.buttons.save_changes')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')
@section('content')
    <div class="col-md-6">
        <div class="card">
            <div class="card-header"><strong>{{__('dashboard.group_category.edit.title')}}: {{$categoryGroup->name}}</strong></div>
            <div class="card-body">
                <form class="form-horizontal" action="{{route('admin.category_groups.update', $categoryGroup)}}"
                      method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="text-center">
                        @if(!$categoryGroup->getFirstMedia('category_group_avatars'))
                            <img src="https://via.placeholder.com/150" style="max-width: 300px" alt="">
                        @else
                            <img src="{{$categoryGroup->getFirstMediaUrl('category_group_avatars')}}" style="max-width: 300px" alt="">
                        @endif
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="name">{{__('dashboard.group_category.edit.name')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="name" type="text" name="name" autocomplete="name"
                                   value="{{old('name', $categoryGroup->name)}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="avatar">{{__('dashboard.group_category.edit.avatar')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="avatar" type="file" name="avatar">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">{{__('dashboard.buttons.save_changes')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-header"><strong>{{__('dashboard.group_category.edit.add_new_category')}}</strong></div>
            <div class="card-body">
                <form class="form-horizontal" action="{{route('admin.categories.store', $categoryGroup->id)}}" method="post">
                    @csrf
                    @method('POST')
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="name">{{__('dashboard.group_category.edit.name')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="name" type="text" name="name" autocomplete="name"
                                   value="{{old('name')}}">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">{{__('dashboard.buttons.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                <strong>{{__('dashboard.categories.index.table_title')}}</strong>
            </div>
            @if(count($categories) == NULL)
                <div class="card-body">{{__('dashboard.global.is_empty')}}</div>
            @else
                <div class="card-body">
                    <table class="table table-responsive-sm">
                        <thead>
                        <tr>
                            <th>{{__('dashboard.categories.index.title')}}</th>
                            <th>{{__('dashboard.global.offers_count')}}</th>
                            <th>{{__('dashboard.global.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->name }}</td>
                                <td>{{ $category->offers_count }}</td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{route('admin.categories.edit', $category)}}">
                                            <svg class="btn btn-outline btn-info"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
                                            </svg>
                                        </a>
                                        <button type="button" class="btn btn-primary"
                                                style="padding: 0; border: 0; display: inline"
                                                data-toggle="modal"
                                                data-target="#deleteCategoryConfirmModal">
                                            <svg class="btn btn-outline btn-danger"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-trash')}}"></use>
                                            </svg>
                                        </button>
                                        @include('admin.group_category.modals.deleteCategoryConfirmModal')

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <div class="card-footer">
                    <div class="pagination">{{ $categories->links() }}</div>
                </div>
            @endif
        </div>
    </div>
@endsection

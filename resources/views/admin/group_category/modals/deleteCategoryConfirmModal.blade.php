<div class="modal fade" tabindex="-1" role="dialog" id="deleteCategoryConfirmModal">
    <form id="delete-form" action="{{route('admin.categories.destroy', $category)}}" method="post">
        @csrf
        @method('DELETE')
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <strong>{{__('dashboard.categories.index.confirmation_delete_category')}}</strong>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">{{__('dashboard.buttons.modals.modal_delete_submit_button')}}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('dashboard.buttons.modals.modal_delete_cancel_button')}}</button>
                </div>
            </div>
        </div>
    </form>
</div>

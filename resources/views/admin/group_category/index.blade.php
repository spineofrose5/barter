@extends('layouts.app')
@section('content')
    <div class="col-md-6">
        <div class="card">
            <div class="card-header"><strong>{{__('dashboard.group_category.create.title')}}</strong></div>
            <div class="card-body">
                <form class="form-horizontal" action="{{route('admin.category_groups.store')}}"
                      method="post" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="name">{{__('dashboard.group_category.create.name')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="name" type="text" name="name" autocomplete="name"
                                   value="{{old('name')}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="avatar">{{__('dashboard.group_category.create.avatar')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="avatar" type="file" name="avatar">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">{{__('dashboard.buttons.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i>
                <strong>{{__('dashboard.group_category.index.table_title')}}</strong>
            </div>
            <div class="card-body">
                @if(! $categoryGroups)
                    <div class="card-body">
                        {{__('dashboard.global.is_empty')}}
                    </div>
                @else
                    <table class="table table-responsive-sm">
                        <thead>
                        <tr>
                            <th>{{__('dashboard.group_category.create.avatar')}}</th>
                            <th>{{__('dashboard.global.title')}}</th>
                            <th>{{__('dashboard.global.categories_count')}}</th>
                            <th>{{__('dashboard.global.offers_count')}}</th>
                            <th>{{__('dashboard.global.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categoryGroups as $categoryGroup)
                            <tr>
                                <td>
                                    <img src="{{$categoryGroup->getFirstMediaUrl('category_group_avatars', 'thumb')}}" alt="">
                                </td>
                                <td>{{ $categoryGroup->name }}</td>
                                <td>{{ $categoryGroup->categories_count }}</td>
                                <td>{{ $categoryGroup->offers_count }}</td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{route('admin.category_groups.edit', $categoryGroup)}}">
                                            <svg class="btn btn-outline btn-info"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
                                            </svg>
                                        </a>
                                        <button type="button" class="btn btn-primary"
                                                style="padding: 0; border: 0; display: inline"
                                                data-toggle="modal"
                                                data-target="#deleteCategoryGroupConfirmModal">
                                            <svg class="btn btn-outline btn-danger"
                                                 style="max-height: 25px; max-width: 25px; padding: 3px">
                                                <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-trash')}}"></use>
                                            </svg>
                                        </button>
                                        @include('admin.group_category.modals.deleteCategoryGroupConfirmModal')

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination">{{ $categoryGroups->links() }}</div>
                @endif
            </div>
        </div>
    </div>

@endsection

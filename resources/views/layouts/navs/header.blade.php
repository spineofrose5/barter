<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
        <svg class="c-icon c-icon-lg">
            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-menu')}}"></use>
        </svg>
    </button><a class="c-header-brand d-lg-none" href="#">
        <svg width="118" height="46" alt="CoreUI Logo">
            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#full')}}"></use>
        </svg></a>
    <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
        <svg class="c-icon c-icon-lg">
            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-menu')}}"></use>
        </svg>
    </button>
    <ul class="c-header-nav d-md-down-none">
        <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">{{__('Dashboard')}}</a></li>
        <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">{{__('Users')}}</a></li>
        <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">{{__('Settings')}}</a></li>
    </ul>
    <ul class="c-header-nav ml-auto mr-4">
        <div class="badge badge-pill badge-success">
{{__('dashboard.subscriptions.index.points')}}: {{auth()->user()->profile->points}}
        </div>
        <li class="c-header-nav-item d-md-down-none mx-2"><a class="c-header-nav-link" href="#">
                <svg class="c-icon">
                    <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-bell')}}"></use>
                </svg></a></li>
        <li class="c-header-nav-item d-md-down-none mx-2"><a class="c-header-nav-link" href="#">
                <svg class="c-icon">
                    <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-list-rich')}}"></use>
                </svg></a></li>
        <li class="c-header-nav-item d-md-down-none mx-2"><a class="c-header-nav-link" href="#">
                <svg class="c-icon">
                    <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-envelope-open')}}"></use>
                </svg></a></li>
        <li class="c-header-nav-item dropdown">
            <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <div class="c-avatar">
                    @if(!auth()->user()->profile || !auth()->user()->profile->getFirstMedia('user_avatars'))
                        <img class="c-avatar-img" src="https://barter-test.s3.eu-west-1.amazonaws.com/icons/user.png"
                             alt="{{auth()->user()->email}}">
                    @else
                        <img class="c-avatar-img" src="{{auth()->user()->profile->getFirstMediaUrl('user_avatars', 'thumb')}}"
                             alt="{{auth()->user()->email}}">
                    @endif
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right pt-0">
                <div class="dropdown-header bg-light py-2"><strong>{{__('Account')}}</strong></div>

                <a class="dropdown-item" href="{{route('auth.profile.edit')}}">

{{--                    <a class="dropdown-item" href="#">--}}
                        <svg class="c-icon mr-2">
                            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-user')}}"></use>
                        </svg>
                    {{__('Profile')}}
                </a>
                <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                        <svg class="c-icon mr-2">
                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-account-logout"></use>
                        </svg>
                        {{__('Log out')}}
                    </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                </a>
            </div>
        </li>
    </ul>
</header>

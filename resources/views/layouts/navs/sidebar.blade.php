<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-lg-down-none">
        <svg class="c-sidebar-brand-full" width="118" height="46" alt="CoreUI Logo">
            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#full')}}"></use>
        </svg>
        <svg class="c-sidebar-brand-minimized" width="46" height="46" alt="CoreUI Logo">
            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#signet')}}"></use>
        </svg>
    </div>
    <ul class="c-sidebar-nav ps">
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{route('home')}}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-speedometer')}}"></use>
                </svg> Dashboard</a></li>
        <li class="c-sidebar-nav-title">{{__('dashboard.global.sidebar.admin_section')}}</li>

        <!-- Users mangement sections -->
        <li class="c-sidebar-nav-item c-sidebar-nav-dropdown  {{ (request()->is('admin/users/*')) ? 'c-show' : '' }}">
            <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-people')}}"></use>
                </svg>
                {{__('dashboard.global.sidebar.user_management')}}
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{route('admin.users.index')}}"><span class="c-sidebar-nav-icon"></span>
                        {{__('dashboard.global.sidebar.users')}}
                    </a>
                </li>
{{--                <li class="c-sidebar-nav-item">--}}
{{--                    <a class="c-sidebar-nav-link" href="{{route('admin.users.index')}}"><span class="c-sidebar-nav-icon"></span>--}}
{{--                        {{__('dashboard.global.roles')}}--}}
{{--                    </a>--}}
{{--                </li>--}}
            </ul>
        </li>
        <!--   Category groups section -->
        <li class="c-sidebar-nav-item c-sidebar-nav-dropdown {{ (request()->is('admin/category_groups/*')) ? 'c-show' : '' }}">
            <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-clear-all')}}"></use>
                </svg>
                {{__('dashboard.global.sidebar.category_groups')}}
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{route('admin.category_groups.index')}}">
                        {{__('dashboard.global.sidebar.category_groups_management')}}
                    </a>
                </li>
                <div class="dropdown-divider"></div>
                @foreach(\App\Models\CategoryGroup::all()->sortBy('name') as $group)
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="{{route('admin.category_groups.edit', $group)}}">
                            {{ $group->name }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </li>

        <!-- Offers section -->
        <li class="c-sidebar-nav-item c-sidebar-nav-dropdown {{ (request()->is('admin/offers/*')) ? 'c-show' : '' }}">
            <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-chart-line')}}"></use>
                </svg>
                {{__('dashboard.global.offers')}}
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{route('admin.offers.index')}}">
                        {{__('dashboard.global.list')}}
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="">
                        {{__("dashboard.global.settings")}}
                    </a>
                </li>
            </ul>
        </li>

        <!-- Transactions section -->
        <li class="c-sidebar-nav-item c-sidebar-nav-dropdown {{ (request()->is('admin/pointsTransactions/*')) ? 'c-show' : '' }}">
            <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-money')}}"></use>
                </svg>
                {{__('dashboard.global.sidebar.transactions')}}
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{route('admin.pointsTransactions.index')}}">
                        {{__('dashboard.global.list')}}
                    </a>
                </li>
            </ul>
        </li>

        <li class="c-sidebar-nav-title">{{__('dashboard.global.sidebar.user_section')}}</li>
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
        <div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div>
    </ul>
    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
</div>

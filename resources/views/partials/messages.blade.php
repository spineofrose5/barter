@if(session('success'))
    <div class="alert alert-success ml-3" role="alert">{!! session('success') !!}</div>
@endif

@if(session('error'))
    <div class="alert alert-danger ml-3" role="alert">{!! session('error') !!}</div>
@endif

@if(session('errors'))
    <div class="alert alert-danger ml-3" role="alert">
        @foreach ($errors->all() as $message)
            <li>{{ $message }}</li>
        @endforeach
    </div>
@endif

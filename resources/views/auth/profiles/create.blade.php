@extends('layouts.app')
@section('content')
    <div class="col-md-6">
        <div class="card">
            <div class="card-header"><strong>{{__('dashboard.profile.edit_form.your_profile')}}</strong></div>
            <div class="card-body">
                <form class="form-horizontal" action="{{route('auth.profile.store')}}"
                      method="post">
                    @csrf
                    @method('POST')
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="first_name">{{__('dashboard.profile.edit_form.first_name')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="first_name" type="text" name="first_name" autocomplete="name"
                                   value="{{old('first_name')}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="surname">{{__('dashboard.profile.edit_form.surname')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="surname" type="text" name="surname" autocomplete="surname"
                                   value="{{old('surname')}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="country">{{__('dashboard.profile.edit_form.country')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="country" type="text" name="country" autocomplete="country"
                                   value="{{old('country')}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="city">{{__('dashboard.profile.edit_form.city')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="city" type="text" name="city" autocomplete="city"
                                   value="{{old('city')}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="postal_code">{{__('dashboard.profile.edit_form.postal_code')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="postal_code" type="text" name="postal_code" autocomplete="postal_code"
                                   value="{{old('postal_code')}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="street">{{__('dashboard.profile.edit_form.street')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="street" type="text" name="street" autocomplete="street"
                                   value="{{old('street')}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="number">{{__('dashboard.profile.edit_form.number')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="number" type="text" name="number" autocomplete="number"
                                   value="{{old('number')}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="contact_phone">{{__('dashboard.profile.edit_form.contact_phone')}}</label>
                        <div class="col-md-9">
                            <input class="form-control" id="contact_phone" type="text" name="contact_phone" autocomplete="contact_phone"
                                   value="{{old('contact_phone')}}">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

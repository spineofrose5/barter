@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-8">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><strong>{{__('dashboard.profile.edit_form.login_information\'s')}}</strong></div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{route('auth.profile.updateLogin', auth()->user())}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="name">{{__('dashboard.profile.edit_form.name')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="name" type="text" name="name" autocomplete="name"
                                       value="{{old('name', auth()->user()->name)}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="email">{{__('dashboard.global.email_address')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="email" type="email" name="email" autocomplete="email"
                                       value="{{old('email', auth()->user()->email)}}">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-primary" type="submit">{{__('dashboard.buttons.save_changes')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><strong>{{__('dashboard.profile.edit_form.change_password')}}</strong></div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{route('auth.profile.updatePassword', auth()->user())}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="current_password">{{__('dashboard.profile.edit_form.current_password')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="current_password" type="password" name="current_password" autocomplete="password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="password">{{__('dashboard.profile.edit_form.password')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="password" type="password" name="password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="password-confirmation">{{__('dashboard.profile.edit_form.confirm_password')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="password-confirm" type="password" name="password_confirmation">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-primary" type="submit">{{__('dashboard.buttons.save_changes')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><strong>{{__('dashboard.profile.edit_form.your_profile')}}</strong></div>
                <div class="card-body">
                    <form class="form-horizontal" action="{{route('auth.profile.updateProfile', auth()->user()->profile)}}"
                          method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="text-center" style="margin-bottom: 10px;">
                            @if(!auth()->user()->profile->getFirstMedia('user_avatars'))
                                <img src="https://lh3.googleusercontent.com/proxy/lLJD7vDKZMqN-fom-JGi2Zq0KDvBowIQhlAbCpSrzCH95YKdcumFb3opp5j7pTxBEnD0dWyvhHA71r2u6VRF5SavdpmcMSMljHla9rHdRcZwXOuJpd_0TaOygDE" style="max-width: 300px" alt="">
                            @else
                                <img src="{{auth()->user()->profile->getFirstMediaUrl('user_avatars')}}" style="max-width: 300px" alt="">
                            @endif
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">{{__('dashboard.profile.edit_form.first_name')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="first_name" type="text" name="first_name" autocomplete="first_name"
                                       value="{{old('first_name', auth()->user()->profile->first_name)}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="surname">{{__('dashboard.profile.edit_form.surname')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="surname" type="text" name="surname" autocomplete="surname"
                                       value="{{old('surname', auth()->user()->profile->surname)}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="country">{{__('dashboard.profile.edit_form.country')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="country" type="text" name="country" autocomplete="country"
                                       value="{{old('country', auth()->user()->profile->country)}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="city">{{__('dashboard.profile.edit_form.city')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="city" type="text" name="city" autocomplete="city"
                                       value="{{old('city', auth()->user()->profile->city)}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="postal_code">{{__('dashboard.profile.edit_form.postal_code')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="postal_code" type="text" name="postal_code" autocomplete="postal_code"
                                       value="{{old('postal_code', auth()->user()->profile->postal_code)}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="street">{{__('dashboard.profile.edit_form.street')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="street" type="text" name="street" autocomplete="street"
                                       value="{{old('street', auth()->user()->profile->street)}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="number">{{__('dashboard.profile.edit_form.number')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="number" type="text" name="number" autocomplete="number"
                                       value="{{old('number', auth()->user()->profile->number)}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="contact_phone">{{__('dashboard.profile.edit_form.contact_phone')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="contact_phone" type="text" name="contact_phone" autocomplete="contact_phone"
                                       value="{{old('contact_phone', auth()->user()->profile->contact_phone)}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="avatar">{{__('dashboard.profile.edit_form.profile_avatar')}}</label>
                            <div class="col-md-9">
                                <input class="form-control" id="avatar" type="file" name="avatar">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-primary" type="submit">{{__('dashboard.buttons.save_changes')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><strong>{{__('dashboard.profile.edit_form.subscription_title')}}</strong></div>
                <div class="card-body">
                    <table class="table table-responsive-sm">
                        <thead>
                            <tr>
                                <th>Nazwa</th>
                                <th>Cena</th>
                                <th>Ilość punktów</th>
                                <th>KUP</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(\App\Models\Subscription::all() as $subscription)
                                <tr>
                                    <td>{{ $subscription->name }}</td>
                                    <td>{{ $subscription->price / 100 }}</td>
                                    <td>{{ $subscription->points }}</td>
                                    <td>
                                        <form action="{{route('getsubscription', $subscription)}}" method="post">
                                            @csrf
                                            @method('POST')

                                            <button type="submit" class="btn btn-sm btn-success">KUP</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

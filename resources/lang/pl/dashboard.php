<?php

return [
    'buttons' => [
        'modals' => [
            'modal_delete_submit_button' => "Tak, usuwam!",
            'modal_delete_cancel_button' => "Odrzuć"
        ],
        'add_button' => 'Dodaj',
        "save" => "Zapisz",
        "edit" => "Edytuj",
        "save_changes" => "Zapisz zmiany",
        "change_role" => "Zmień rolę",
        "delete_button" => "Usuń"
    ],
    'global' => [
        'sidebar' => [
            'admin_section' => 'Administracja',
            'user_section' => 'Strefa użytkownika',
            'user_management' => 'Zarządzanie użytkownikami',
            'users' => 'Lista użytkowników',
            'category_groups' => 'Grupy kategorii',
            'category_groups_management' => 'Zarządzanie grupami',
            'transactions' => 'Transakcje',
        ],
        'errors' => [
            'dont_find' => 'Nie znaleziono obiektu'
        ],
        'buy' => 'KUP',
        'dont_enough_points' => 'Masz za mało punktów',
        'created_at' => 'Dodano',
        'created_by_user' => 'Dodano przez',
        'in_category' => 'W kategorii',
        'subscription' => 'Abonament',
        'not_applicable' => 'Nie dotyczy',
        'description' => 'Opis',
        'list' => 'Lista',
        'all' => 'Wszystkie',
        'actions'=> 'Akcje',
        'email_address' => 'Adres email',
        'users' => 'Użytkownicy',
        'username' => 'Nazwa użytkownika',
        'name' => 'Imię i nazwisko',
        'address' => 'Pełny adres',
        'points' => 'Punkty',
        'role' => 'Rola',
        'roles' => 'Role',
        'status' => 'Status',
        'statuses' => 'Statusy',
        'no_profile_message' => 'Musisz uzupełnić swój profil',
        'offer' => 'Oferta',
        'offers' => 'Oferty',
        'user_offers' => 'Ogłoszenia użytkownika',
        'offers_count' => 'Ilość ogłoszeń',
        'categories_count' => 'Ilość kategorii',
        'title' => 'Tytuł',
        'quantity' => 'Ilość',
        'category_name' => 'Nazwa kategorii',
        'coast' => 'Koszt w punktach',
        'payment_type' => 'Forma zapłaty',
        'exchange_for' => 'Na co wymienić?',
        'is_empty' => 'Nie ma obiektów do pokazania',
        'settings' => 'Ustawienia',
        'offer_type' => 'Rodzaj oferty',
        'date' => 'Data',
        'photos' => 'Zdjęcia',
    ],
    'profile' => [
        'edit_form' => [
            'login_information\'s' => 'Dane logowania',
            'name' => 'Twoja nazwa użytkownika',
            'current_password' => 'Obecne hasło',
            'password' => 'Nowe hasło',
            'confirm_password' => 'Potwierdź hasło',
            'change_password' => 'Zmień hasło',
            'your_profile' => 'Twój profil',
            'first_name' => 'Imię',
            'surname' => 'Nazwisko',
            'country' => 'Kraj',
            'city' => 'Miasto',
            'postal_code' => 'Kod pocztowy',
            'street' => 'Ulica',
            'number' => 'Numer domu/mieszkania',
            'contact_phone' => 'Numer kontaktowy',
            'profile_avatar' => 'Zdjęcie użytkownika',
            'subscription_title' => 'Kup abonament'
        ]
    ],
    'users' => [
        'index' => [
            'confirmation_delete_user' => "Jeśteś pewien? Usunięcie sprawi usunięcie użytkownika razem z ogłoszeniami i historią.",
        ],
        'create_form' => [

        ],
        'edit_form' => [
            'user_profile' => 'Profil użytkownika',
            'name' => 'Nazwa użytkownika',
            'change_role_label' => 'Wybierz nową rolę dla użytkownika',
            'change_status_label' => 'Wybierz status użytkownika',
            'generate_password_button' => "Generuj nowe hasło"
        ],
        'show' => [

        ],
    ],
    'group_category' => [
        'index' => [
            'table_title' => 'Lista grup kategorii',
            'confirmation_delete_category_group' => "Jeśteś pewien? Usunięcie sprawi usunięcie grupy razem z kategoriami i ogłoszeniami! Może zmiana nazwy wystarczy?.",
            'title' => 'Nazwa kategorii'
        ],
        'create' => [
            'title' => 'Utwórz grupę kategorii',
            'name' => 'Nazwa grupy kategorii',
            'avatar' => 'Ikona grupy kategorii'
        ],
        'edit' => [
            'title' => 'Grupa kategorii',
            'name' => 'Nazwa grupy kategorii',
            'avatar' => 'Ikona grupy kategorii',
            'add_new_category' => 'Dodaj nową kategorię do grupy'
        ]
    ],
    'categories' => [
        'index' => [
            'table_title' => 'Lista kategorii',
            'confirmation_delete_category' => "Jeśteś pewien? Usunięcie sprawi usunięcie kategorii z ogłoszeniami!",
            'title' => 'Nazwa kategorii',
        ],
        'create' => [
            'title' => 'Utwórz kategorię',
            'name' => 'Nazwa kategorii',
        ],
        'edit' => [
            'title' => 'Edytuj kategorię',
            'name' => 'Nazwa kategorii',
        ]
    ],
    'offers' => [
        'index' => [
            'table_title' => 'Lista ofert',
            'confirmation_delete_offer' => "Jeśteś pewien?",
            'title' => 'Nazwa oferty',
            'avatar' => 'Foto',
            'description' => 'Opis',

        ],
        'create' => [
            'title' => 'Utwórz kategorię',
            'name' => 'Nazwa kategorii',
        ],
        'edit' => [
            'title' => 'Edytuj ofertę',
            'name' => 'Tytuł oferty',
            'description' => 'Opis oferty',
            'change_status_label' => 'Zmień status oferty',
            'published_at' => 'Data publikacji',
            'confirmation_delete_media_modal' => 'Usuwasz permanentnie zdjęcie z oferty. Jesteś pewien?'
        ],
        'show' => [

        ],
    ],
    'points_transactions' => [
        'index' => [
            'title' => 'Lista transakcji punktowych',
            'title_second_table' => 'Lista kupionych abonamentów',
            'transaction_type' => 'Rodzaj transakcji',
            'amount' => 'Ilość punktów',
            'user_from' => 'Od użytkownika',
            'user_to' => 'Do użytkownika',
            'created_at' => 'Data utworzenia',
            'completed_at' => 'Data zakończenia',
            'confirmation_delete_transaction' => 'Czy jesteś pewien? Transakcja bedzie dostępna w archiwum.'
        ]
    ],
    'subscriptions' => [
        'index' => [
            'title' => 'Abonamenty',
            'name' => 'Nazwa abonamentu',
            'price' => 'Cena',
            'points' => 'Liczba punktów',
        ]
    ],
    'settings' => [

    ],
];

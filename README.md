# Barter
Aplikacja do barterowego zbywania towarów / usług. Użytkownik ma możliwość nabycia lub zbycia towaru i usługi w zamian za towar / usługę (wymiana), nabycie lub zbycie towaru lub usługi nieodpłatnie (za uśmiech :) ) lub transakcja może zostać sfinalizowana dzięki punktom wewnątrz aplikacji.

**Aplikacja w dużym stopniu jest dwujęzyczna na obecnym etapie rozwoju. Obsługiwane języki to polski i angielski.**

### Punkty - wewnętrzna forma płatności
Punkty nabywane są za pośrednictwem aplikacji w formie określonej ich ilości, wykupienia abonamentu, który daje również inne korzyści, lub po odsprzedaniu towaru / usługi za punkty, które otrzymujemy od kupującego.
# Założenia techniczne
## Profil użytkownika
Użytkownik posiada profil z podstawowymi informacjami. Jego utworzenie po udanej rejestracji jest konieczne, bez tego nie przejdziemy dalej, nie zrobimy kompletnie nic w panelu aplikacji.
Dane w profilu to:

- first_name *,   [3-20 znaków]
- surname *,   [3-20 znaków]
- country,  [3-20 znaków]
- city,   [3-20 znaków]
- postal_code,    [3-10 znaków]
- street,   [3-30 znaków]
- number,   [1-5 znaków]
- contact_phone *   [unikalny]
- każdy profil posiada nieobowiązkowy avatar [1mb, jpeg, jpg, png, svg]
  *pole obowiązkowe

Oprócz profilu, przy rejestracji, użytkownik podaje nazwę (login), email i hasło.

**Administrator nie musi posiadać profilu, chyba, że korzysta z aplikacji jako użytkownik - wymienia towary / usługi!**

### Role
W systemie funkcjonują rolę użytkownik, moderator, administrator. Zmiany roli dokonać może tego tylko administrator systemu.

Po rejestracji użytkownik otrzymuje rolę "user".

## Statusy
Każde konto użytkownika może być: aktywne, nieaktywne, zawieszone. Administrator i moderator mają możliwość zmiany 
statusu konta w dowolnym momencie. Zawieszenie jest czasowe za złamanie regulaminu.

Każda oferta może być: aktywne, nieaktywne, zrealizowana. Administrator i moderator mają możliwość zmiany
statusu oferty w dowolnym momencie. 


## Kategorie i grupy kategorii ogłoszeń
Aby skutecznie zarządzać ogłoszeniami aplikacja pozwala tworzyć kategorie produktów oraz grupy kategorii [dostępne z poziomu administratora]

Kategoria zawiera w bazie danych tylko `nazwę [3-20 znaków]`, natomiast grupa kategorii zawiera również `avatar kategorii [1mb, jpg, jpeg, png, svg]`.


### Grupy kategorii
Aplikacja zawiera podstawowe grupy kategorii, takie jak: 'elekronika', 'moda', 'motoryzacja' etc.

### Kategorie
Każda grupa zawiera konkretne kategorie będące jej 'dziećmi' np 'motoryzacja' > 'samochody osobowe' etc.
Kategoria w bazie danych zawiera:

## Ogłoszenia

W przykładowej bazie danych znajduje się 200 ogłoszeń, przypisanych do 50 różnych kategorii, te z kolei są przypisane do grup kategorii (7)

Ogłoszenie składa się z:
 - title *   [3-150 znaków]
 - description *   [w html]
 - payment_type *  [do wyboru: za darmo, za punkty, na wymiane]
 - coast   [w punktach, 1-9999 cyfr]
 - exchange_for    [za co wymienić, 255znaków]
 - offer_type *   [do wyboru: przedmiot, usługa, reklama]
  *pole obowiązkowe

Każde ogłoszenie zawierać może zdjęcia [2-8], które są zmniejszane automatycznie.

## Transakcje punktowe

Transakcje to osobna tabela, która zawiera pola:
 - transaction_type [rodzaj transakcji: incoming (nie ma możliwości wpłaty punktów, ale można je kupić, np z abonamentem), outgoing]
 - transfer_from [może być puste - w przypadku transferu punktów tu zapisze się login użytkownika od którego mamy punkty]
 - transfer_to [może być puste - w przypadku transferu punktów tu zapisze się login użytkownika do którego wysłaliśmy punkty]
 - description [może być puste - krótki opis transakcji]
 - completed_at [może być puste, data zakończenia transakcji]  
 - offer_id [powiązanie z ofertą jakiej dotyczy transakcja (o ile dotyczyu oferty)]
 - amount [wartość transakcji]

Każda transakcja ma przypisany status w innej tabeli:
 - Pending
 - Success
 - Error

## Stack technologiczny
- PHP > 7.3
- Laravel > 8.4

[comment]: <> (- Livewire)

### Paczki użyte w aplikacji

-  **[laravel-model-status](https://github.com/spatie/laravel-model-status)**
-  **[laravel-medialibrary](https://github.com/spatie/laravel-medialibrary)**
-  **[coreui-free-bootstrap-admin-template](https://github.com/coreui/coreui-free-bootstrap-admin-template)**
 
# Instalacja
Aby lokalnie zainstalować aplikację należy(zakładamy, że posiadacie zainstalowane takie aplikacje, jak npm, composer, php):
Sklonować repozytorium używając GITa a następnie w katalogu głównym:

1. W konsoli: `composer install` oraz `npm install && npm run dev` - co pozwoli zainstalować zależności oraz zainstalować i skompilować style aplikacji
2. Skopiować `.env.example` jako `.env` i wypełnić swoimi danymi - najważniejsze dane to połączenie do bazy danych, bez tego nie wystartujemy. Następnie w konsoli wydajemy polecenie: `php artisan key:generate` by wygenerować unikalny klucz aplikacji
3. Aby nasza baza miała potrzebne tabele wydajemy polecenie: `php artisan migrate` oraz aby zyskać przykładowe dane: `php artisan db:seed`

